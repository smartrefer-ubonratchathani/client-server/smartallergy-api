const StationListSchema = {
  querystring: {
    type: "object",
    properties: {
      province_code: { type: "string" },
      amphur_code: { type: "string" },
      tambol_code: { type: "string" },
    },
    required: [
      "province_code"
    ]
  }
}

const CreateStationSchema = {
  body: {
    type: "object",
    additionalProperties: false,
    properties: {
      station_code: { type: "string" },
      location: { type: "string" },
      level: { type: "string", enum: ["1", "2", "3"] },
      lat: { type: "number" },
      lng: { type: "number" },
      status: { type: "boolean" },
      staff: {
        type: "array",
        minItems: 1,
        items: {
          type: "object",
          properties: {
            staff_name: { type: "string" },
            staff_phone: { type: "string" }
          }
        }
      },
    },
    required: [
      "station_code",
      "location",
      "level",
      "lat",
      "lng",
      "staff",
    ]
  }
}

export { StationListSchema, CreateStationSchema }