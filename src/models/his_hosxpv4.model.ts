import { Knex } from 'knex';

export class HisHosxpv4Model {

    async getPerson(db: Knex) {
        // ชื่อผู้ป่วย
        // return [{hospital_code:'',cid:'',title:'',fname:'',lname:''}]
        let data = await db.raw(`
        SELECT (SELECT sys_value  from sys_var WHERE sys_name = "hospital_code" LIMIT 1) as hospital_code
        ,pt.cid
           ,pt.pname as title
           ,pt.fname
           ,pt.lname
        FROM patient pt 
        WHERE pt.hn in (SELECT oa.hn FROM opd_allergy oa group by oa.hn)
               AND pt.hn not in ( SELECT dt.hn FROM death dt GROUP BY dt.hn)
               AND pt.nationality = "99"
    UNION
        SELECT (SELECT sys_value  from sys_var WHERE sys_name = "hospital_code" LIMIT 1) as hospital_code
            ,pt.cid
           ,pt.pname as title
           ,pt.fname
           ,pt.lname
        from opitemrece op 
        INNER JOIN patient pt on pt.hn = op.hn AND pt.nationality = "99"
        INNER JOIN (SELECT d.icode,d.name as drug_name,d.tpu_code_list as tmt_id,d.did as std_code
        FROM drugitems d
        WHERE d.sks_drug_code in ("208802","208818","208825","219068","219075","219081","196258","196262","196270","196289","107576","107582","107595","107609")
        ) dd on dd.icode = op.icode
               AND pt.hn not in ( SELECT dt.hn FROM death dt GROUP BY dt.hn)
        GROUP BY cid
    `);
        return data[0];
    }
    
    async getAllergy(db: Knex) {
        // แพ้ยา
        // return [{hospital_code:'',drug_name:'',allergy_level_id:'',report_date:'',tmt_id:'',std_code:'',symptom:'',cid:''}]
        let data = await db.raw(`
        SELECT (SELECT sys_value  from sys_var WHERE sys_name = "hospital_code" LIMIT 1) as hospital_code
        ,oa.agent as drug_name
        ,oa.allergy_relation_id as allergy_level_id
        ,oa.report_date
           ," " as tmt_id
        ," " as std_code
        ,oa.symptom
           ,pt.cid
        FROM patient pt 
        INNER JOIN opd_allergy oa on oa.hn = pt.hn
        WHERE pt.hn not in ( SELECT dt.hn FROM death dt GROUP BY dt.hn)
               AND pt.nationality = "99"
    `);
        return data[0];
    }
    
    async getDrugs(db: Knex) {
        // return [{hospital_code:'',drug_name:'',tmt_id:'',std_code:'',use_date:'',cid:''}]
        let data = await db.raw(`
        SELECT (SELECT sys_value  from sys_var WHERE sys_name = "hospital_code" LIMIT 1) as hospital_code
           ,dd.drug_name
           ,dd.tmt_id
           ,dd.std_code
           ,op.vstdate as use_date
           ,pt.cid
        from opitemrece op 
        INNER JOIN patient pt on pt.hn = op.hn AND pt.nationality = "99"
        INNER JOIN (SELECT d.icode,d.name as drug_name,d.tpu_code_list as tmt_id,d.did as std_code
        FROM drugitems d
        WHERE d.sks_drug_code in ("208802","208818","208825","219068","219075","219081","196258","196262","196270","196289","107576","107582","107595","107609")
        ) dd on dd.icode = op.icode
               AND pt.hn not in ( SELECT dt.hn FROM death dt GROUP BY dt.hn)
    `);
        return data[0];
    }
    
    async getG6pd(db: Knex) {
        // return [{cid:''}]
        let data = await db.raw(`
        SELECT pt.cid
        FROM opd_allergy oa
        INNER JOIN patient pt on pt.hn = oa.hn AND pt.nationality = "99"
        WHERE (SUBSTR(LTRIM(REPLACE(oa.agent,"-",'')),1,4) = "G6PD"
             OR SUBSTR(LTRIM(REPLACE(oa.symptom,"-",'')),1,4) = "G6PD")
               AND pt.hn not in ( SELECT dt.hn FROM death dt GROUP BY dt.hn)
        GROUP BY oa.hn
    `);
        return data[0];
    }

    async getPersonToday(db: Knex) {
        // ชื่อผู้ป่วย
        // return [{hospital_code:'',cid:'',title:'',fname:'',lname:''}]
        let data = await db.raw(`
        SELECT (SELECT sys_value  from sys_var WHERE sys_name = "hospital_code" LIMIT 1) as hospital_code
            ,pt.cid
               ,pt.pname as title
               ,pt.fname
               ,pt.lname
        FROM patient pt 
        WHERE pt.hn in (SELECT oa.hn FROM opd_allergy oa WHERE oa.report_date = (Date_Format((DATE_SUB(NOW(),INTERVAL 1 DAY)),"%Y-%m-%d")) group by oa.hn)
               AND pt.hn not in ( SELECT dt.hn FROM death dt GROUP BY dt.hn)
               AND pt.nationality = "99"
    UNION
        SELECT (SELECT sys_value  from sys_var WHERE sys_name = "hospital_code" LIMIT 1) as hospital_code
           ,pt.cid
               ,pt.pname as title
               ,pt.fname
               ,pt.lname
        from opitemrece op 
        INNER JOIN patient pt on pt.hn = op.hn AND pt.nationality = "99"
        INNER JOIN (SELECT d.icode,d.name as drug_name,d.tpu_code_list as tmt_id,d.did as std_code
        FROM drugitems d
        WHERE d.sks_drug_code in ("208802","208818","208825","219068","219075","219081","196258","196262","196270","196289","107576","107582","107595","107609")
        ) dd on dd.icode = op.icode
               AND op.vstdate = (Date_Format((DATE_SUB(NOW(),INTERVAL 1 DAY)),"%Y-%m-%d"))
               AND pt.hn not in ( SELECT dt.hn FROM death dt GROUP BY dt.hn)
        GROUP BY cid
    `);
        return data[0];
    }
    
    async getAllergyToday(db: Knex) {
        // แพ้ยา
        // return [{hospital_code:'',drug_name:'',allergy_level_id:'',report_date:'',tmt_id:'',std_code:'',symptom:'',cid:''}]
        let data = await db.raw(`
        SELECT (SELECT sys_value  from sys_var WHERE sys_name = "hospital_code" LIMIT 1) as hospital_code
            ,oa.agent as drug_name
            ,oa.allergy_relation_id as allergy_level_id
            ,oa.report_date
               ," " as tmt_id
            ," " as std_code
            ,oa.symptom
               ,pt.cid
        FROM patient pt 
        INNER JOIN opd_allergy oa on oa.hn = pt.hn
        WHERE  oa.report_date = (Date_Format((DATE_SUB(NOW(),INTERVAL 1 DAY)),"%Y-%m-%d")) 
                   AND pt.hn not in ( SELECT dt.hn FROM death dt GROUP BY dt.hn)
                   AND pt.nationality = "99"
    `);
        return data[0];
    }
    
    async getDrugsToday(db: Knex) {
        // return [{hospital_code:'',drug_name:'',tmt_id:'',std_code:'',use_date:'',cid:''}]
        let data = await db.raw(`
        SELECT (SELECT sys_value  from sys_var WHERE sys_name = "hospital_code" LIMIT 1) as hospital_code
           ,dd.drug_name
           ,dd.tmt_id
           ,dd.std_code
           ,op.vstdate as use_date
           ,pt.cid
        from opitemrece op 
        INNER JOIN patient pt on pt.hn = op.hn AND pt.nationality = "99"
        INNER JOIN (SELECT d.icode,d.name as drug_name,d.tpu_code_list as tmt_id,d.did as std_code
        FROM drugitems d
        WHERE d.sks_drug_code in ("208802","208818","208825","219068","219075","219081","196258","196262","196270","196289","107576","107582","107595","107609")
        ) dd on dd.icode = op.icode
               AND op.vstdate = (Date_Format((DATE_SUB(NOW(),INTERVAL 1 DAY)),"%Y-%m-%d")) 
               AND pt.hn not in ( SELECT dt.hn FROM death dt GROUP BY dt.hn)
    `);
        return data[0];
    }
    
    async getG6pdToday(db: Knex) {
        // return [{cid:''}]
        let data = await db.raw(`
        SELECT pt.cid
        FROM opd_allergy oa
        INNER JOIN patient pt on pt.hn = oa.hn AND pt.nationality = "99"
        WHERE (SUBSTR(LTRIM(REPLACE(oa.agent,"-",'')),1,4) = "G6PD"
                  OR SUBSTR(LTRIM(REPLACE(oa.symptom,"-",'')),1,4) = "G6PD")
               AND oa.report_date = (Date_Format((DATE_SUB(NOW(),INTERVAL 1 DAY)),"%Y-%m-%d")) 
               AND pt.hn not in ( SELECT dt.hn FROM death dt GROUP BY dt.hn)
        GROUP BY oa.hn
    `);
        return data[0];
    }
    async getPersonDate(db: Knex,datestart:any,dateend:any) {
        // ชื่อผู้ป่วย
        // return [{hospital_code:'',cid:'',title:'',fname:'',lname:''}]
        let data = await db.raw(`
        SELECT (SELECT sys_value  from sys_var WHERE sys_name = "hospital_code" LIMIT 1) as hospital_code
            ,pt.cid
               ,pt.pname as title
               ,pt.fname
               ,pt.lname
        FROM patient pt 
        WHERE pt.hn in (SELECT oa.hn FROM opd_allergy oa WHERE date(oa.report_date) betwen '${datestart}' and '${dateend}' group by oa.hn)
               AND pt.hn not in ( SELECT dt.hn FROM death dt GROUP BY dt.hn)
               AND pt.nationality = "99"
    UNION
        SELECT (SELECT sys_value  from sys_var WHERE sys_name = "hospital_code" LIMIT 1) as hospital_code
           ,pt.cid
               ,pt.pname as title
               ,pt.fname
               ,pt.lname
        from opitemrece op 
        INNER JOIN patient pt on pt.hn = op.hn AND pt.nationality = "99"
        INNER JOIN (SELECT d.icode,d.name as drug_name,d.tpu_code_list as tmt_id,d.did as std_code
        FROM drugitems d
        WHERE d.sks_drug_code in ("208802","208818","208825","219068","219075","219081","196258","196262","196270","196289","107576","107582","107595","107609")
        ) dd on dd.icode = op.icode
               AND date(op.vstdate) betwen '${datestart}' and '${dateend}'
               AND pt.hn not in ( SELECT dt.hn FROM death dt GROUP BY dt.hn)
        GROUP BY cid
    `);
        return data[0];
    }
    
    async getAllergyDate(db: Knex,datestart:any,dateend:any) {
        // แพ้ยา
        // return [{hospital_code:'',drug_name:'',allergy_level_id:'',report_date:'',tmt_id:'',std_code:'',symptom:'',cid:''}]
        let data = await db.raw(`
        SELECT (SELECT sys_value  from sys_var WHERE sys_name = "hospital_code" LIMIT 1) as hospital_code
            ,oa.agent as drug_name
            ,oa.allergy_relation_id as allergy_level_id
            ,oa.report_date
               ," " as tmt_id
            ," " as std_code
            ,oa.symptom
               ,pt.cid
        FROM patient pt 
        INNER JOIN opd_allergy oa on oa.hn = pt.hn
        WHERE  date(oa.report_date) =  betwen '${datestart}' and '${dateend}' 
                   AND pt.hn not in ( SELECT dt.hn FROM death dt GROUP BY dt.hn)
                   AND pt.nationality = "99"
    `);
        return data[0];
    }
    
    async getDrugsDate(db: Knex,datestart:any,dateend:any) {
        // return [{hospital_code:'',drug_name:'',tmt_id:'',std_code:'',use_date:'',cid:''}]
        let data = await db.raw(`
        SELECT (SELECT sys_value  from sys_var WHERE sys_name = "hospital_code" LIMIT 1) as hospital_code
           ,dd.drug_name
           ,dd.tmt_id
           ,dd.std_code
           ,op.vstdate as use_date
           ,pt.cid
        from opitemrece op 
        INNER JOIN patient pt on pt.hn = op.hn AND pt.nationality = "99"
        INNER JOIN (SELECT d.icode,d.name as drug_name,d.tpu_code_list as tmt_id,d.did as std_code
        FROM drugitems d
        WHERE d.sks_drug_code in ("208802","208818","208825","219068","219075","219081","196258","196262","196270","196289","107576","107582","107595","107609")
        ) dd on dd.icode = op.icode
               AND date(op.vstdate) betwen '${datestart}' and '${dateend}'
               AND pt.hn not in ( SELECT dt.hn FROM death dt GROUP BY dt.hn)
    `);
        return data[0];
    }
    
}