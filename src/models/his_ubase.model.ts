import { Knex } from 'knex';

export class HisUbaseModel {


    async getPerson(db: Knex) {
        // ชื่อผู้ป่วย
        // return [{hospital_code:'',cid:'',title:'',fname:'',lname:''}]
        let data = await db.raw(`
        SELECT '15078' as hospital_code, 
            pop.cid as cid , 
            '' as title, pop.fname as fname, 
            pop.lname as lname 
            FROM cid_drug_allergy a , population pop
            WHERE a.cid = pop.CID GROUP BY a.CID 
        UNION
            select '15078' as hospital_code, 
            pop.cid as cid , 
            '' as title, 
            pop.fname as fname, 
            pop.lname as lname 
            from opd_visits a , prescriptions pst , cid_hn c, population pop ,tmt15078 tmt 
            WHERE tmt.tmtid in ('208802','208818','208825','219068','219075','219081','196258','196262','196270','196289','107576','107582','107595','107609') 
            AND tmt.drug_id = pst.drug_id AND pst.visit_id = a.visit_id AND a.hn = c.hn AND c.cid = pop.cid AND a.is_cancel = 0
        `);
        return data[0];

    }

    async getAllergy(db: Knex) {
        // แพ้ยา
        // return [{hospital_code:'',drug_name:'',allergy_level_id:'',report_date:'',tmt_id:'',std_code:'',symptom:'',cid:''}]
        let data = await db.raw(`
            SELECT 
                '15078' AS hospital_code,
                a.cid AS cid,
                d.drug_name AS drug_name,
                a.level as allergy_level_id,
                a.upd_dt as report_date,       
                '' AS tmt_id,
                '' as std_code,
                a.allergy_note as symptom
                FROM cid_drug_allergy a  
                INNER JOIN drugs d ON a.drug_id = d.drug_id
                WHERE a.is_cancel = 0 
        `);
        return data[0];

    }

    async getDrugs(db: Knex) {
        // return [{hospital_code:'',drug_name:'',tmt_id:'',std_code:'',use_date:'',cid:''}]
        let data = await db.raw(`
        SELECT
            '15078' as hospital_code,
            d.drug_name as drug_name,
            tmt.tmtid AS tmt_id,
            tmt.ndc24 as std_code,
            pt.reg_datetime as use_date,
            a.cid as cid
            FROM cid_drug_allergy a
            INNER JOIN drugs d ON a.drug_id = d.drug_id
            INNER JOIN tmt15078 tmt ON d.drug_id = tmt.drug_id
            INNER JOIN cid_hn c ON a.cid = c.cid
            INNER JOIN opd_visits pt ON c.hn = pt.hn
            INNER JOIN prescriptions pst ON pt.visit_id = pst.visit_id
            WHERE tmt.tmtid in ('208802','208818','208825','219068','219075','219081','196258','196262','196270','196289','107576','107582','107595','107609')
            AND a.is_cancel = 0
    `);

    }

    async getG6pd(db: Knex) {
        // return [{cid:''}]
        let data = await db.raw(`
        SELECT a.cid AS cid
    			FROM cid_drug_allergy a
                INNER JOIN cid_hn b ON a.cid = b.cid 
                INNER JOIN opd_visits pt ON b.hn = pt.hn
                INNER JOIN opd_diagnosis od ON pt.visit_id = od.VISIT_ID
                INNER JOIN icd10new icd ON od.icd10 = icd.icd10
                WHERE od.ICD10 IN (SELECT icd10 FROM icd10new WHERE icd_name LIKE '%g6pd%'OR nickname LIKE '%g6pd%')           
                AND a.IS_CANCEL = 0 AND pt.is_cancel = 0 AND od.IS_CANCEL = 0
    `);
        return data[0];
    }

    async getPersonToday(db: Knex) {
        // ชื่อผู้ป่วย
        // return [{hospital_code:'',cid:'',title:'',fname:'',lname:''}]
        let data = await db.raw(`
        SELECT '15078' as hospital_code, 
            pop.cid as cid , 
            '' as title, pop.fname as fname, 
            pop.lname as lname 
            FROM cid_drug_allergy a , population pop
            WHERE DATE_FORMAT(a.upd_dt,'%Y-%m-%d') = (DATE_FORMAT((DATE_SUB(NOW() , INTERVAL 1 DAY )) , '%Y-%m-%d')) 
            AND a.cid = pop.CID GROUP BY a.CID 
        UNION
            select '15078' as hospital_code, 
            pop.cid as cid , 
            '' as title, 
            pop.fname as fname, 
            pop.lname as lname 
            from opd_visits a , prescriptions pst , cid_hn c, population pop ,tmt15078 tmt 
            WHERE DATE_FORMAT(a.reg_datetime,'%Y-%m-%d') = (DATE_FORMAT((DATE_SUB(NOW() , INTERVAL 1 DAY )) , '%Y-%m-%d'))
            AND tmt.tmtid in ('208802','208818','208825','219068','219075','219081','196258','196262','196270','196289','107576','107582','107595','107609') 
            AND tmt.drug_id = pst.drug_id AND pst.visit_id = a.visit_id AND a.hn = c.hn AND c.cid = pop.cid AND a.is_cancel = 0
        `);
        return data[0];
    }
    
    async getAllergyToday(db: Knex) {
        // แพ้ยา
        // return [{hospital_code:'',drug_name:'',allergy_level_id:'',report_date:'',tmt_id:'',std_code:'',symptom:'',cid:''}]
        let data = await db.raw(`
        SELECT 
                '15078' AS hospital_code,
                a.cid AS cid,
                d.drug_name AS drug_name,
                a.level as allergy_level_id,
                a.upd_dt as report_date,       
                '' AS tmt_id,
                '' as std_code,
                a.allergy_note as symptom
                FROM cid_drug_allergy a  
                INNER JOIN drugs d ON a.drug_id = d.drug_id
                WHERE DATE_FORMAT(a.upd_dt, '%Y-%m-%d') = (DATE_FORMAT((DATE_SUB(NOW() , INTERVAL 1 DAY )) , '%Y-%m-%d'))
                AND a.is_cancel = 0 
        `);
        return data[0];
    }
    
    async getDrugsToday(db: Knex) {
        // return [{hospital_code:'',drug_name:'',tmt_id:'',std_code:'',use_date:'',cid:''}]
        let data = await db.raw(`
        SELECT
            '15078' as hospital_code,
            d.drug_name as drug_name,
            tmt.tmtid AS tmt_id,
            tmt.ndc24 as std_code,
            pt.reg_datetime as use_date,
            a.cid as cid
            FROM cid_drug_allergy a
            INNER JOIN drugs d ON a.drug_id = d.drug_id
            INNER JOIN tmt15078 tmt ON d.drug_id = tmt.drug_id
            INNER JOIN cid_hn c ON a.cid = c.cid
            INNER JOIN opd_visits pt ON c.hn = pt.hn
            INNER JOIN prescriptions pst ON pt.visit_id = pst.visit_id
            WHERE DATE_FORMAT(pt.reg_datetime,'%Y-%m-%d') = (DATE_FORMAT((DATE_SUB(NOW() , INTERVAL 1 DAY )) , '%Y-%m-%d'))
            AND tmt.tmtid in ('208802','208818','208825','219068','219075','219081','196258','196262','196270','196289','107576','107582','107595','107609')
            AND a.is_cancel = 0
        `);
        return data[0];
    }
    
    async getG6pdToday(db: Knex) {
        // return [{cid:''}]
        let data = await db.raw(`
        SELECT a.cid AS cid
        FROM cid_drug_allergy a
        INNER JOIN cid_hn b ON a.cid = b.cid 
        INNER JOIN opd_visits pt ON b.hn = pt.hn
        INNER JOIN opd_diagnosis od ON pt.visit_id = od.VISIT_ID
        INNER JOIN icd10new icd ON od.icd10 = icd.icd10
        WHERE od.ICD10 IN (SELECT icd10 FROM icd10new WHERE icd_name LIKE '%g6pd%'OR nickname LIKE '%g6pd%') 
        AND DATE_FORMAT(a.upd_dt,'%Y-%m-%d')= (DATE_FORMAT((DATE_SUB(NOW() , INTERVAL 1 DAY )) , '%Y-%m-%d'))          
        AND a.IS_CANCEL = 0 AND pt.is_cancel = 0 AND od.IS_CANCEL = 0
        `);
        return data[0];
    }

    async getPersonDate(db: Knex,datestart:any,dateend:any) {
        // ชื่อผู้ป่วย
        // return [{hospital_code:'',cid:'',title:'',fname:'',lname:''}]
        let data = await db.raw(`
        SELECT '15078' as hospital_code, 
            pop.cid as cid , 
            '' as title, pop.fname as fname, 
            pop.lname as lname 
            FROM cid_drug_allergy a , population pop
            WHERE DATE_FORMAT(a.upd_dt,'%Y-%m-%d') between '${datestart}' and '${dateend}' 
            AND a.cid = pop.CID GROUP BY a.CID 
        UNION
            select '15078' as hospital_code, 
            pop.cid as cid , 
            '' as title, 
            pop.fname as fname, 
            pop.lname as lname 
            from opd_visits a , prescriptions pst , cid_hn c, population pop ,tmt15078 tmt 
            WHERE DATE_FORMAT(a.reg_datetime,'%Y-%m-%d') between '${datestart}' and '${dateend}' 
            AND tmt.tmtid in ('208802','208818','208825','219068','219075','219081','196258','196262','196270','196289','107576','107582','107595','107609') 
            AND tmt.drug_id = pst.drug_id AND pst.visit_id = a.visit_id AND a.hn = c.hn AND c.cid = pop.cid AND a.is_cancel = 0
        `);
        return data[0];
    }
    
    async getAllergyDate(db: Knex,datestart:any,dateend:any) {
        // แพ้ยา
        // return [{hospital_code:'',drug_name:'',allergy_level_id:'',report_date:'',tmt_id:'',std_code:'',symptom:'',cid:''}]
        let data = await db.raw(`
        SELECT 
                '15078' AS hospital_code,
                a.cid AS cid,
                d.drug_name AS drug_name,
                a.level as allergy_level_id,
                a.upd_dt as report_date,       
                '' AS tmt_id,
                '' as std_code,
                a.allergy_note as symptom
                FROM cid_drug_allergy a  
                INNER JOIN drugs d ON a.drug_id = d.drug_id
                WHERE DATE_FORMAT(a.upd_dt, '%Y-%m-%d') between '${datestart}' and '${dateend}' 
                AND a.is_cancel = 0 
        `);
        return data[0];
    }
    
    async getDrugsDate(db: Knex,datestart:any,dateend:any) {
        // return [{hospital_code:'',drug_name:'',tmt_id:'',std_code:'',use_date:'',cid:''}]
        let data = await db.raw(`
        SELECT
            '15078' as hospital_code,
            d.drug_name as drug_name,
            tmt.tmtid AS tmt_id,
            tmt.ndc24 as std_code,
            pt.reg_datetime as use_date,
            a.cid as cid
            FROM cid_drug_allergy a
            INNER JOIN drugs d ON a.drug_id = d.drug_id
            INNER JOIN tmt15078 tmt ON d.drug_id = tmt.drug_id
            INNER JOIN cid_hn c ON a.cid = c.cid
            INNER JOIN opd_visits pt ON c.hn = pt.hn
            INNER JOIN prescriptions pst ON pt.visit_id = pst.visit_id
            WHERE DATE_FORMAT(pt.reg_datetime,'%Y-%m-%d') between '${datestart}' and '${dateend}' 
            AND tmt.tmtid in ('208802','208818','208825','219068','219075','219081','196258','196262','196270','196289','107576','107582','107595','107609')
            AND a.is_cancel = 0
        `);
        return data[0];
    }

}

