import { Knex } from 'knex';

export class HisMbaseModel {

    async getPerson(db: Knex) {
        // ชื่อผู้ป่วย
        // return [{hospital_code:'',cid:'',title:'',fname:'',lname:''}]
        let data = await db.raw(`
        SELECT g.offid as hospital_code, c.cid ,
        CASE
                    WHEN p.PRENAME not in('') THEN TRIM(p.PRENAME)
                        #WHEN TIMESTAMPDIFF(year,pv.BIRTHDATE,NOW())< '20' AND pv.sex='1' AND pv.MARRIAGE = '4'THEN 'สามเณร'
                        #WHEN TIMESTAMPDIFF(year,pv.BIRTHDATE,NOW()) >= '20' AND pv.sex='1' AND pv.MARRIAGE  = '4'THEN 'พระภิกษุ'
                        WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='1' THEN 'ด.ช.'
                        WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='1' THEN 'นาย'
                        WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='2' THEN 'ด.ญ.'
                        WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='2' AND p.MARRIAGE ='1' THEN 'น.ส.'
                        ELSE 'นาง' 
        END  as title,
        trim(p.fname) as fname, trim(p.lname) as lname
        FROM gcoffice g, opd_visits o
        INNER JOIN cid_hn c ON c.hn = o.hn 
        INNER JOIN population p ON p.cid = c.cid
        LEFT JOIN prescriptions ps ON ps.visit_id = o.visit_id  AND ps.is_cancel = 0
        LEFT JOIN drugs d ON d.drug_id = ps.drug_id
        WHERE d.drug_id = '2274'
        UNION ALL
        SELECT g.offid as hospital_code, c.cid ,
        CASE
                    WHEN p.PRENAME not in('') THEN TRIM(p.PRENAME)
                        #WHEN TIMESTAMPDIFF(year,pv.BIRTHDATE,NOW())< '20' AND pv.sex='1' AND pv.MARRIAGE = '4'THEN 'สามเณร'
                        #WHEN TIMESTAMPDIFF(year,pv.BIRTHDATE,NOW()) >= '20' AND pv.sex='1' AND pv.MARRIAGE  = '4'THEN 'พระภิกษุ'
                        WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='1' THEN 'ด.ช.'
                        WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='1' THEN 'นาย'
                        WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='2' THEN 'ด.ญ.'
                        WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='2' AND p.MARRIAGE ='1' THEN 'น.ส.'
                        ELSE 'นาง' 
        END  as title,
        trim(p.fname) as fname, trim(p.lname) as lname
                FROM gcoffice g , cid_drug_allergy c 
                INNER JOIN population p ON p.cid = c.cid
                INNER  JOIN drugs d ON d.drug_id = c.drug_id AND c.is_cancel = 0
                WHERE 
                c.cid NOT IN (SELECT de.cid FROM deaths de) 
        UNION ALL
        SELECT g.offid as hospital_code, b.cid ,
        CASE
                    WHEN p.PRENAME not in('') THEN TRIM(p.PRENAME)
                        #WHEN TIMESTAMPDIFF(year,pv.BIRTHDATE,NOW())< '20' AND pv.sex='1' AND pv.MARRIAGE = '4'THEN 'สามเณร'
                        #WHEN TIMESTAMPDIFF(year,pv.BIRTHDATE,NOW()) >= '20' AND pv.sex='1' AND pv.MARRIAGE  = '4'THEN 'พระภิกษุ'
                        WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='1' THEN 'ด.ช.'
                        WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='1' THEN 'นาย'
                        WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='2' THEN 'ด.ญ.'
                        WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='2' AND p.MARRIAGE ='1' THEN 'น.ส.'
                        ELSE 'นาง' 
        END  as title, trim(p.fname) as fname, trim(p.lname) as lname
            FROM gcoffice g,cid_drug_allergy a
            INNER JOIN cid_hn b ON a.cid = b.cid
            INNER JOIN population p ON p.cid = b.cid
            INNER JOIN opd_visits pt ON b.hn = pt.hn
            INNER JOIN opd_diagnosis od ON pt.visit_id = od.VISIT_ID
            INNER JOIN icd10new icd ON od.icd10 = icd.icd10
            WHERE od.ICD10 IN (SELECT icd10 FROM icd10new WHERE icd_name LIKE '%g6pd%'OR nickname LIKE '%g6pd%')           
            AND a.IS_CANCEL = 0 AND pt.is_cancel = 0 AND od.IS_CANCEL = 0
    
        `);
        return data[0];
    }
    
    async getAllergy(db: Knex) {
        // แพ้ยา
        // return [{hospital_code:'',drug_name:'',allergy_level_id:'',report_date:'',tmt_id:'',std_code:'',symptom:'',cid:''}]
        let data = await db.raw(`
        SELECT g.offid as hospital_code,  d.drug_name, c.LEVEL as allergy_level_id, date(c.upd_dt) as report_date,
		'' as tmt_id, d.didstd as std_code, c.ALLERGY_NOTE as symptom , c.cid
		FROM gcoffice g , cid_drug_allergy c 
		INNER  JOIN drugs d ON d.drug_id = c.drug_id AND c.is_cancel = 0
		 WHERE 
		  c.cid NOT IN (SELECT de.cid FROM deaths de) 
        `);
        return data[0];
    }
    
    async getDrugs(db: Knex) {
        // return [{hospital_code:'',drug_name:'',tmt_id:'',std_code:'',use_date:'',cid:''}]
        let data = await db.raw(`
        SELECT g.offid as hospital_code, d.drug_name , '' as tmt_id, d.didstd as std_code, date(o.reg_datetime) as use_date, c.cid
		FROM gcoffice g, opd_visits o
		INNER JOIN cid_hn c ON c.hn = o.hn 
		INNER JOIN population p ON p.cid = c.cid
		LEFT JOIN prescriptions ps ON ps.visit_id = o.visit_id  AND ps.is_cancel = 0
		LEFT JOIN drugs d ON d.drug_id = ps.drug_id
		WHERE d.drug_id = '2274'
        `);
        return data[0];
    }
    
    async getG6pd(db: Knex) {
        // return [{cid:''}]
        let data = await db.raw(`
        SELECT a.cid AS cid
	FROM cid_drug_allergy a
	INNER JOIN cid_hn b ON a.cid = b.cid 
	INNER JOIN opd_visits pt ON b.hn = pt.hn
	INNER JOIN opd_diagnosis od ON pt.visit_id = od.VISIT_ID
	INNER JOIN icd10new icd ON od.icd10 = icd.icd10
	WHERE od.ICD10 IN (SELECT icd10 FROM icd10new WHERE icd_name LIKE '%g6pd%'OR nickname LIKE '%g6pd%')           
	AND a.IS_CANCEL = 0 AND pt.is_cancel = 0 AND od.IS_CANCEL = 0
        `);
        return data[0];
    }
    async getPersonToday(db: Knex) {
        // ชื่อผู้ป่วย
        // return [{hospital_code:'',cid:'',title:'',fname:'',lname:''}]
        let data = await db.raw(`
        SELECT g.offid as hospital_code, c.cid ,
CASE
            WHEN p.PRENAME not in('') THEN TRIM(p.PRENAME)
                #WHEN TIMESTAMPDIFF(year,pv.BIRTHDATE,NOW())< '20' AND pv.sex='1' AND pv.MARRIAGE = '4'THEN 'สามเณร'
                #WHEN TIMESTAMPDIFF(year,pv.BIRTHDATE,NOW()) >= '20' AND pv.sex='1' AND pv.MARRIAGE  = '4'THEN 'พระภิกษุ'
                WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='1' THEN 'ด.ช.'
                WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='1' THEN 'นาย'
                WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='2' THEN 'ด.ญ.'
                WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='2' AND p.MARRIAGE ='1' THEN 'น.ส.'
                ELSE 'นาง' 
END  as title,
 trim(p.fname) as fname, trim(p.lname) as lname
FROM gcoffice g, opd_visits o
INNER JOIN cid_hn c ON c.hn = o.hn 
INNER JOIN population p ON p.cid = c.cid
LEFT JOIN prescriptions ps ON ps.visit_id = o.visit_id  AND ps.is_cancel = 0
LEFT JOIN drugs d ON d.drug_id = ps.drug_id
WHERE date(o.reg_datetime) = CURRENT_DATE - 1
AND d.drug_id = '2274'
UNION ALL
SELECT g.offid as hospital_code, c.cid ,
CASE
            WHEN p.PRENAME not in('') THEN TRIM(p.PRENAME)
                #WHEN TIMESTAMPDIFF(year,pv.BIRTHDATE,NOW())< '20' AND pv.sex='1' AND pv.MARRIAGE = '4'THEN 'สามเณร'
                #WHEN TIMESTAMPDIFF(year,pv.BIRTHDATE,NOW()) >= '20' AND pv.sex='1' AND pv.MARRIAGE  = '4'THEN 'พระภิกษุ'
                WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='1' THEN 'ด.ช.'
                WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='1' THEN 'นาย'
                WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='2' THEN 'ด.ญ.'
                WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='2' AND p.MARRIAGE ='1' THEN 'น.ส.'
                ELSE 'นาง' 
END  as title,
 trim(p.fname) as fname, trim(p.lname) as lname
		FROM gcoffice g , cid_drug_allergy c 
		INNER JOIN population p ON p.cid = c.cid
		INNER  JOIN drugs d ON d.drug_id = c.drug_id AND c.is_cancel = 0
		 WHERE 
		  date(c.upd_dt) = CURRENT_DATE - 1
		  AND c.cid NOT IN (SELECT de.cid FROM deaths de) 
UNION ALL
 SELECT g.offid as hospital_code, b.cid ,
CASE
            WHEN p.PRENAME not in('') THEN TRIM(p.PRENAME)
                #WHEN TIMESTAMPDIFF(year,pv.BIRTHDATE,NOW())< '20' AND pv.sex='1' AND pv.MARRIAGE = '4'THEN 'สามเณร'
                #WHEN TIMESTAMPDIFF(year,pv.BIRTHDATE,NOW()) >= '20' AND pv.sex='1' AND pv.MARRIAGE  = '4'THEN 'พระภิกษุ'
                WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='1' THEN 'ด.ช.'
                WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='1' THEN 'นาย'
                WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='2' THEN 'ด.ญ.'
                WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='2' AND p.MARRIAGE ='1' THEN 'น.ส.'
                ELSE 'นาง' 
END  as title, trim(p.fname) as fname, trim(p.lname) as lname
	FROM gcoffice g,cid_drug_allergy a
	INNER JOIN cid_hn b ON a.cid = b.cid
	INNER JOIN population p ON p.cid = b.cid
	INNER JOIN opd_visits pt ON b.hn = pt.hn
	INNER JOIN opd_diagnosis od ON pt.visit_id = od.VISIT_ID
	INNER JOIN icd10new icd ON od.icd10 = icd.icd10
	WHERE od.ICD10 IN (SELECT icd10 FROM icd10new WHERE icd_name LIKE '%g6pd%'OR nickname LIKE '%g6pd%')           
	AND date(a.upd_dt)= CURRENT_DATE - 1
	AND a.IS_CANCEL = 0 AND pt.is_cancel = 0 AND od.IS_CANCEL = 0
        `);
        return data[0];
    }
    
    async getAllergyToday(db: Knex) {
        // แพ้ยา
        // return [{hospital_code:'',drug_name:'',allergy_level_id:'',report_date:'',tmt_id:'',std_code:'',symptom:'',cid:''}]
        let data = await db.raw(`
        SELECT g.offid as hospital_code,  d.drug_name, c.LEVEL as allergy_level_id, date(c.upd_dt) as report_date,
		'' as tmt_id, d.didstd as std_code, c.ALLERGY_NOTE as symptom , c.cid
		FROM gcoffice g , cid_drug_allergy c 
		INNER  JOIN drugs d ON d.drug_id = c.drug_id AND c.is_cancel = 0
		 WHERE 
		  date(c.upd_dt) = CURRENT_DATE - 1
		  AND c.cid NOT IN (SELECT de.cid FROM deaths de) 
        `);
        return data[0];
    }
    
    async getDrugsToday(db: Knex) {
        // return [{hospital_code:'',drug_name:'',tmt_id:'',std_code:'',use_date:'',cid:''}]
        let data = await db.raw(`
        SELECT g.offid as hospital_code, d.drug_name , '' as tmt_id, d.didstd as std_code, date(o.reg_datetime) as use_date, c.cid
		FROM gcoffice g, opd_visits o
		INNER JOIN cid_hn c ON c.hn = o.hn 
		INNER JOIN population p ON p.cid = c.cid
		LEFT JOIN prescriptions ps ON ps.visit_id = o.visit_id  AND ps.is_cancel = 0
		LEFT JOIN drugs d ON d.drug_id = ps.drug_id
		WHERE date(o.reg_datetime) = CURRENT_DATE - 1
		AND d.drug_id = '2274'
        `);
        return data[0];
    }
    
    async getG6pdToday(db: Knex) {
        // return [{cid:''}]
        let data = await db.raw(`
        SELECT a.cid AS cid
        FROM cid_drug_allergy a
        INNER JOIN cid_hn b ON a.cid = b.cid 
        INNER JOIN opd_visits pt ON b.hn = pt.hn
        INNER JOIN opd_diagnosis od ON pt.visit_id = od.VISIT_ID
        INNER JOIN icd10new icd ON od.icd10 = icd.icd10
        WHERE od.ICD10 IN (SELECT icd10 FROM icd10new WHERE icd_name LIKE '%g6pd%'OR nickname LIKE '%g6pd%')           
        AND date(a.upd_dt)= CURRENT_DATE - 1
        AND a.IS_CANCEL = 0 AND pt.is_cancel = 0 AND od.IS_CANCEL = 0
        `);
        return data[0];
    }

    async getPersonDate(db: Knex,datestart:any,dateend:any) {
        // ชื่อผู้ป่วย
        // return [{hospital_code:'',cid:'',title:'',fname:'',lname:''}]
        let data = await db.raw(`
        SELECT g.offid as hospital_code, c.cid ,
CASE
            WHEN p.PRENAME not in('') THEN TRIM(p.PRENAME)
                #WHEN TIMESTAMPDIFF(year,pv.BIRTHDATE,NOW())< '20' AND pv.sex='1' AND pv.MARRIAGE = '4'THEN 'สามเณร'
                #WHEN TIMESTAMPDIFF(year,pv.BIRTHDATE,NOW()) >= '20' AND pv.sex='1' AND pv.MARRIAGE  = '4'THEN 'พระภิกษุ'
                WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='1' THEN 'ด.ช.'
                WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='1' THEN 'นาย'
                WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='2' THEN 'ด.ญ.'
                WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='2' AND p.MARRIAGE ='1' THEN 'น.ส.'
                ELSE 'นาง' 
END  as title,
 trim(p.fname) as fname, trim(p.lname) as lname
FROM gcoffice g, opd_visits o
INNER JOIN cid_hn c ON c.hn = o.hn 
INNER JOIN population p ON p.cid = c.cid
LEFT JOIN prescriptions ps ON ps.visit_id = o.visit_id  AND ps.is_cancel = 0
LEFT JOIN drugs d ON d.drug_id = ps.drug_id
WHERE date(o.reg_datetime) = CURRENT_DATE - 1
AND d.drug_id = '2274'
UNION ALL
SELECT g.offid as hospital_code, c.cid ,
CASE
            WHEN p.PRENAME not in('') THEN TRIM(p.PRENAME)
                #WHEN TIMESTAMPDIFF(year,pv.BIRTHDATE,NOW())< '20' AND pv.sex='1' AND pv.MARRIAGE = '4'THEN 'สามเณร'
                #WHEN TIMESTAMPDIFF(year,pv.BIRTHDATE,NOW()) >= '20' AND pv.sex='1' AND pv.MARRIAGE  = '4'THEN 'พระภิกษุ'
                WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='1' THEN 'ด.ช.'
                WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='1' THEN 'นาย'
                WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='2' THEN 'ด.ญ.'
                WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='2' AND p.MARRIAGE ='1' THEN 'น.ส.'
                ELSE 'นาง' 
END  as title,
 trim(p.fname) as fname, trim(p.lname) as lname
		FROM gcoffice g , cid_drug_allergy c 
		INNER JOIN population p ON p.cid = c.cid
		INNER  JOIN drugs d ON d.drug_id = c.drug_id AND c.is_cancel = 0
		 WHERE 
		  date(c.upd_dt) between '${datestart}' and '${dateend}' 
		  AND c.cid NOT IN (SELECT de.cid FROM deaths de) 
UNION ALL
 SELECT g.offid as hospital_code, b.cid ,
CASE
            WHEN p.PRENAME not in('') THEN TRIM(p.PRENAME)
                #WHEN TIMESTAMPDIFF(year,pv.BIRTHDATE,NOW())< '20' AND pv.sex='1' AND pv.MARRIAGE = '4'THEN 'สามเณร'
                #WHEN TIMESTAMPDIFF(year,pv.BIRTHDATE,NOW()) >= '20' AND pv.sex='1' AND pv.MARRIAGE  = '4'THEN 'พระภิกษุ'
                WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='1' THEN 'ด.ช.'
                WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='1' THEN 'นาย'
                WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='2' THEN 'ด.ญ.'
                WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='2' AND p.MARRIAGE ='1' THEN 'น.ส.'
                ELSE 'นาง' 
END  as title, trim(p.fname) as fname, trim(p.lname) as lname
	FROM gcoffice g,cid_drug_allergy a
	INNER JOIN cid_hn b ON a.cid = b.cid
	INNER JOIN population p ON p.cid = b.cid
	INNER JOIN opd_visits pt ON b.hn = pt.hn
	INNER JOIN opd_diagnosis od ON pt.visit_id = od.VISIT_ID
	INNER JOIN icd10new icd ON od.icd10 = icd.icd10
	WHERE od.ICD10 IN (SELECT icd10 FROM icd10new WHERE icd_name LIKE '%g6pd%'OR nickname LIKE '%g6pd%')           
	AND date(a.upd_dt) between '${datestart}' and '${dateend}' 
	AND a.IS_CANCEL = 0 AND pt.is_cancel = 0 AND od.IS_CANCEL = 0
        `);
        return data[0];
    }
    
    async getAllergyDate(db: Knex,datestart:any,dateend:any) {
        // แพ้ยา
        // return [{hospital_code:'',drug_name:'',allergy_level_id:'',report_date:'',tmt_id:'',std_code:'',symptom:'',cid:''}]
        let data = await db.raw(`
        SELECT g.offid as hospital_code,  d.drug_name, c.LEVEL as allergy_level_id, date(c.upd_dt) as report_date,
		'' as tmt_id, d.didstd as std_code, c.ALLERGY_NOTE as symptom , c.cid
		FROM gcoffice g , cid_drug_allergy c 
		INNER  JOIN drugs d ON d.drug_id = c.drug_id AND c.is_cancel = 0
		 WHERE 
		  date(c.upd_dt) between '${datestart}' and '${dateend}' 
		  AND c.cid NOT IN (SELECT de.cid FROM deaths de) 
        `);
        return data[0];
    }
    
    async getDrugsDate(db: Knex,datestart:any,dateend:any) {
        // return [{hospital_code:'',drug_name:'',tmt_id:'',std_code:'',use_date:'',cid:''}]
        let data = await db.raw(`
        SELECT g.offid as hospital_code, d.drug_name , '' as tmt_id, d.didstd as std_code, date(o.reg_datetime) as use_date, c.cid
		FROM gcoffice g, opd_visits o
		INNER JOIN cid_hn c ON c.hn = o.hn 
		INNER JOIN population p ON p.cid = c.cid
		LEFT JOIN prescriptions ps ON ps.visit_id = o.visit_id  AND ps.is_cancel = 0
		LEFT JOIN drugs d ON d.drug_id = ps.drug_id
		WHERE date(o.reg_datetime) between '${datestart}' and '${dateend}' 
		AND d.drug_id = '2274'
        `);
        return data[0];
    }
    


}

