import { Knex } from 'knex';


export class HisHimproHiModel {

    async getPerson(db: Knex) {
        // ชื่อผู้ป่วย
        // return [{hospital_code:'',cid:'',title:'',fname:'',lname:''}]
        let data = await db.raw(`
            SELECT 
            pt.ptallergy.pcucode as hospital_code,
            replace(pt.cardid, "-", "") as cid,
            pt.pt.pttitle as title,
            pt.pt.ptfname as fname,
            pt.pt.ptlname as lname
            from pt.ptallergy
            INNER JOIN pt.pt on pt.ptallergy.hn = pt.hn
            where pt.ptallergy.typeallergy !='ALG0'
            UNION
            SELECT 
            opd.drug_order_opd.pcucode as hospital_code,
            replace(pt.cardid, "-", "") as cid,
            pt.pt.pttitle as title,
            pt.pt.ptfname as fname,
            pt.pt.ptlname as lname
            from opd.drug_order_opd
            INNER JOIN pt.pt on opd.drug_order_opd.hn = pt.pt.hn
            INNER JOIN hos.itemlist on opd.drug_order_opd.codedrug = hos.itemlist.itemcode
            where hos.itemlist.tmt_code in (
            '208802',
            '208818',
            '208825',
            '219068',
            '219075',
            '219081',
            '196258',
            '196262',
            '196270',
            '196289',
            '107576',
            '107582',
            '107595',
            '107609'
            );  
        `);
        return data[0];
    }

    async getAllergy(db: Knex) {
        // แพ้ยา
        // return [{hospital_code:'',drug_name:'',allergy_level_id:'',report_date:'',tmt_id:'',std_code:'',symptom:'',cid:''}]
        let data = await db.raw(`
            SELECT 
            pt.ptallergy.pcucode as hospital_code,
            pt.ptallergy.listname as drug_name,
            pt.ptallergy.typeallergy as allergy_level_id,
            pt.ptallergy.daterecord as report_date,
            hos.itemlist.tmt_code as tmt_id,
            hos.itemto24code.std_24code as std_code,
            pt.ptallergy.listsign as symptom,
            replace(pt.cardid, "-", "") as cid
            FROM pt.ptallergy 
            INNER JOIN hos.itemlist on pt.ptallergy.listcode = hos.itemlist.itemcode
            INNER JOIN hos.itemto24code on hos.itemlist.itemcode = hos.itemto24code.drugcode
            INNER JOIN pt.pt on pt.ptallergy.hn = pt.hn
        `);
        return data[0];
    }

    async getDrugs(db: Knex) {
        // return [{hospital_code:'',drug_name:'',tmt_id:'',std_code:'',use_date:'',cid:''}]
        let data = await db.raw(`
            SELECT 
            opd.drug_order_opd.pcucode as hospital_code,
            opd.drug_order_opd.namedrug as drug_name,
            hos.itemlist.tmt_code as tmt_id,
            hos.itemto24code.std_24code as std_code,
            opd.drug_order_opd.regdate as use_date,
            replace(pt.cardid, "-", "") as cid
            FROM opd.drug_order_opd 
            INNER JOIN hos.itemlist on opd.drug_order_opd.codedrug = hos.itemlist.itemcode
            INNER JOIN hos.itemto24code on hos.itemlist.itemcode = hos.itemto24code.drugcode
            INNER JOIN pt.pt on opd.drug_order_opd.hn = pt.hn
            where hos.itemlist.tmt_code in(
            '208802',
            '208818',
            '208825',
            '219068',
            '219075',
            '219081',
            '196258',
            '196262',
            '196270',
            '196289',
            '107576',
            '107582',
            '107595',
            '107609'
            );
        `);
        return data[0];
    }

    async getG6pd(db: Knex) {
        // return [{cid:''}]
        let data = await db.raw(`select '' as cid`);
        return data[0];
    }


    async getPersonToday(db: Knex) {
        // ชื่อผู้ป่วย
        // return [{hospital_code:'',cid:'',title:'',fname:'',lname:''}]
        let data = await db.raw(`
            SELECT 
            pt.ptallergy.pcucode as hospital_code,
            replace(pt.cardid, "-", "") as cid,
            pt.pt.pttitle as title,
            pt.pt.ptfname as fname,
            pt.pt.ptlname as lname
            from pt.ptallergy
            INNER JOIN pt.pt on pt.ptallergy.hn = pt.hn
            where pt.ptallergy.daterecord =  date(SUBDATE(NOW(),INTERVAL 1 day))
            and pt.ptallergy.typeallergy !='ALG0'
            UNION
            SELECT 
            opd.drug_order_opd.pcucode as hospital_code,
            replace(pt.cardid, "-", "") as cid,
            pt.pt.pttitle as title,
            pt.pt.ptfname as fname,
            pt.pt.ptlname as lname
            from opd.drug_order_opd
            INNER JOIN pt.pt on opd.drug_order_opd.hn = pt.pt.hn
            INNER JOIN hos.itemlist on opd.drug_order_opd.codedrug = hos.itemlist.itemcode
            where opd.drug_order_opd.regdate = date(SUBDATE(now(), INTERVAL 1 day))
            and hos.itemlist.tmt_code in (
            '208802',
            '208818',
            '208825',
            '219068',
            '219075',
            '219081',
            '196258',
            '196262',
            '196270',
            '196289',
            '107576',
            '107582',
            '107595',
            '107609'
            );
        `);
        return data[0];
    }

    async getAllergyToday(db: Knex) {
        // แพ้ยา
        // return [{hospital_code:'',drug_name:'',allergy_level_id:'',report_date:'',tmt_id:'',std_code:'',symptom:'',cid:''}]
        let data = await db.raw(`
            SELECT 
            pt.ptallergy.pcucode as hospital_code,
            pt.ptallergy.listname as drug_name,
            pt.ptallergy.typeallergy as allergy_level_id,
            pt.ptallergy.daterecord as report_date,
            hos.itemlist.tmt_code as tmt_id,
            hos.itemto24code.std_24code as std_code,
            pt.ptallergy.listsign as symptom,
            replace(pt.cardid, "-", "") as cid
            FROM pt.ptallergy 
            INNER JOIN hos.itemlist on pt.ptallergy.listcode = hos.itemlist.itemcode
            INNER JOIN hos.itemto24code on hos.itemlist.itemcode = hos.itemto24code.drugcode
            INNER JOIN pt.pt on pt.ptallergy.hn = pt.hn
            INNER JOIN opd.drug_order_opd on pt.pt.hn = opd.drug_order_opd.hn
            where opd.drug_order_opd.regdate = date(SUBDATE(now(), INTERVAL 1 day))
        `);
        return data[0];
    }

    async getDrugsToday(db: Knex) {
        // return [{hospital_code:'',drug_name:'',tmt_id:'',std_code:'',use_date:'',cid:''}]
        let data = await db.raw(`
            SELECT 
            opd.drug_order_opd.pcucode as hospital_code,
            opd.drug_order_opd.namedrug as drug_name,
            hos.itemlist.tmt_code as tmt_id,
            hos.itemto24code.std_24code as std_code,
            opd.drug_order_opd.regdate as use_date,
            replace(pt.cardid, "-", "") as cid
            FROM opd.drug_order_opd 
            INNER JOIN hos.itemlist on opd.drug_order_opd.codedrug = hos.itemlist.itemcode
            INNER JOIN hos.itemto24code on hos.itemlist.itemcode = hos.itemto24code.drugcode
            INNER JOIN pt.pt on opd.drug_order_opd.hn = pt.hn
            where hos.itemlist.tmt_code in(
            '208802',
            '208818',
            '208825',
            '219068',
            '219075',
            '219081',
            '196258',
            '196262',
            '196270',
            '196289',
            '107576',
            '107582',
            '107595',
            '107609'
            and opd.drug_order_opd.regdate = date(SUBDATE(now(), INTERVAL 1 day))
            );
        `);
        return data[0];
    }

    async getG6pdToday(db: Knex) {
        // return [{cid:''}]
        let data = await db.raw(`select '' as cid`);
        return data[0];
    }

    async getPersonDate(db: Knex,datestart:any,dateend:any) {
        // ชื่อผู้ป่วย
        // return [{hospital_code:'',cid:'',title:'',fname:'',lname:''}]
        let data = await db.raw(`
            SELECT 
            pt.ptallergy.pcucode as hospital_code,
            replace(pt.cardid, "-", "") as cid,
            pt.pt.pttitle as title,
            pt.pt.ptfname as fname,
            pt.pt.ptlname as lname
            from pt.ptallergy
            INNER JOIN pt.pt on pt.ptallergy.hn = pt.hn
            where pt.ptallergy.daterecord =  date(SUBDATE(NOW(),INTERVAL 1 day))
            and pt.ptallergy.typeallergy !='ALG0'
            UNION
            SELECT 
            opd.drug_order_opd.pcucode as hospital_code,
            replace(pt.cardid, "-", "") as cid,
            pt.pt.pttitle as title,
            pt.pt.ptfname as fname,
            pt.pt.ptlname as lname
            from opd.drug_order_opd
            INNER JOIN pt.pt on opd.drug_order_opd.hn = pt.pt.hn
            INNER JOIN hos.itemlist on opd.drug_order_opd.codedrug = hos.itemlist.itemcode
            where opd.drug_order_opd.regdate between '${datestart}' and '${dateend}'
            and hos.itemlist.tmt_code in (
            '208802',
            '208818',
            '208825',
            '219068',
            '219075',
            '219081',
            '196258',
            '196262',
            '196270',
            '196289',
            '107576',
            '107582',
            '107595',
            '107609'
            );
        `);
        return data[0];
    }

    async getAllergyDate(db: Knex,datestart:any,dateend:any) {
        // แพ้ยา
        // return [{hospital_code:'',drug_name:'',allergy_level_id:'',report_date:'',tmt_id:'',std_code:'',symptom:'',cid:''}]
        let data = await db.raw(`
            SELECT 
            pt.ptallergy.pcucode as hospital_code,
            pt.ptallergy.listname as drug_name,
            pt.ptallergy.typeallergy as allergy_level_id,
            pt.ptallergy.daterecord as report_date,
            hos.itemlist.tmt_code as tmt_id,
            hos.itemto24code.std_24code as std_code,
            pt.ptallergy.listsign as symptom,
            replace(pt.cardid, "-", "") as cid
            FROM pt.ptallergy 
            INNER JOIN hos.itemlist on pt.ptallergy.listcode = hos.itemlist.itemcode
            INNER JOIN hos.itemto24code on hos.itemlist.itemcode = hos.itemto24code.drugcode
            INNER JOIN pt.pt on pt.ptallergy.hn = pt.hn
            INNER JOIN opd.drug_order_opd on pt.pt.hn = opd.drug_order_opd.hn
            where opd.drug_order_opd.regdate  between '${datestart}' and '${dateend}'
        `);
        return data[0];
    }

    async getDrugsDate(db: Knex,datestart:any,dateend:any) {
        // return [{hospital_code:'',drug_name:'',tmt_id:'',std_code:'',use_date:'',cid:''}]
        let data = await db.raw(`
            SELECT 
            opd.drug_order_opd.pcucode as hospital_code,
            opd.drug_order_opd.namedrug as drug_name,
            hos.itemlist.tmt_code as tmt_id,
            hos.itemto24code.std_24code as std_code,
            opd.drug_order_opd.regdate as use_date,
            replace(pt.cardid, "-", "") as cid
            FROM opd.drug_order_opd 
            INNER JOIN hos.itemlist on opd.drug_order_opd.codedrug = hos.itemlist.itemcode
            INNER JOIN hos.itemto24code on hos.itemlist.itemcode = hos.itemto24code.drugcode
            INNER JOIN pt.pt on opd.drug_order_opd.hn = pt.hn
            where hos.itemlist.tmt_code in(
            '208802',
            '208818',
            '208825',
            '219068',
            '219075',
            '219081',
            '196258',
            '196262',
            '196270',
            '196289',
            '107576',
            '107582',
            '107595',
            '107609'
            and opd.drug_order_opd.regdate between '${datestart}' and '${dateend}'
            );
        `);
        return data[0];
    }    
}